package com.weather.utils

const val CITYNAME = "cityName"
const val TEMP = "TEMP"
const val TEMP_FEELS_LIKE = "temp_feels_like"
const val WEATHER = "weather"
const val WEATHER_DATA = "weather_data"