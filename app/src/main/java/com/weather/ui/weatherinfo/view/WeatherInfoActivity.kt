package com.weather.ui.weatherinfo.view

import android.os.Bundle
import com.weather.base.BaseActivity
import com.weather.databinding.ActivityWeatherinfoBinding
import com.weather.ui.weatherinfo.viewmodel.WeatherInfoViewModel
import com.weather.utils.*

class WeatherInfoActivity : BaseActivity() {
    private lateinit var viewModel: WeatherInfoViewModel
    private lateinit var mbinding : ActivityWeatherinfoBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mbinding = ActivityWeatherinfoBinding.inflate(layoutInflater)
        setContentView(mbinding.root)
        setUpViewModel()
        setUpUI()
        setupTitle()
    }

    private fun setupTitle(){
        supportActionBar?.title = intent.getStringExtra(CITYNAME)?:""
    }

    private fun setUpViewModel(){
        viewModel = WeatherInfoViewModel()
    }

    private fun setUpUI(){
        mbinding.tempLabel.text =  intent.getDoubleExtra(TEMP,0.0)
            .convertKelvinToFarenhit().toString()
        mbinding.feelsLikeLabel.text =  "Feels Like " +
                "${intent.getDoubleExtra(TEMP_FEELS_LIKE,0.0).convertKelvinToFarenhit()}"
        mbinding.weatherName.text =   intent.getStringExtra(WEATHER)
        mbinding.weatherDescription.text =  intent.getStringExtra(WEATHER_DATA)
    }
}