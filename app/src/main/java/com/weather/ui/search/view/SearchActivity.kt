package com.weather.ui.search.view

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.weather.R
import com.weather.base.BaseActivity
import com.weather.databinding.ActivitySearchBinding
import com.weather.ui.search.viewmodel.SearchViewModel
import com.weather.ui.weather.view.WeatherActivity
import com.weather.utils.CITYNAME
import com.weather.utils.showToast

class SearchActivity : BaseActivity() {
    private lateinit var mbinding : ActivitySearchBinding
    private lateinit var mViewModel: SearchViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mbinding = ActivitySearchBinding.inflate(layoutInflater)
        setContentView(mbinding.root)
        setUpViewModel()
        setUpView()
    }

    private fun setUpViewModel(){
        mViewModel = SearchViewModel()
    }

    private fun setUpView(){
        mbinding.lookUp.setOnClickListener {
            if(mbinding.editSearchCity.text.toString().isNotEmpty()){
                val intent = Intent(this@SearchActivity, WeatherActivity::class.java)
                intent.putExtra(CITYNAME,mbinding.editSearchCity.text.toString())
                startActivity(intent)
            }else{
                showToast(this, resources.getString(R.string.city_name))
            }
        }
    }
}