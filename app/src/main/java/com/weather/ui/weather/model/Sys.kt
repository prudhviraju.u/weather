package com.weather.ui.weather.model

data class Sys(
    val pod: String
)