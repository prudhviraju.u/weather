package com.weather.ui.weather.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.weather.data.api.Resource
import com.weather.data.api.RetrofitBuilder
import com.weather.ui.weather.model.DataList
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class WeatherViewModel(cityName:String) : ViewModel() {
    private val weatherLiveData = MutableLiveData<Resource<List<DataList>>>()

    init {
        getWeatherData(cityName)
    }

    private fun  getWeatherData(cityName: String){
        viewModelScope.launch(Dispatchers.IO) {
            val retrofit = RetrofitBuilder.getInstance()
            weatherLiveData.postValue(Resource.loading(null))
            try {
                val weatherData = retrofit.getForeCast(
                    cityName,
                    "65d00499677e59496ca2f318eb68c049"
                )
                weatherLiveData.postValue(Resource.success(weatherData.list))
            }catch (e:Exception){
                weatherLiveData.postValue(Resource.error(e.toString(), null))
            }
        }
    }

    fun getWeatherData(): LiveData<Resource<List<DataList>>> {
        return weatherLiveData
    }
}