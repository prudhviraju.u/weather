package com.weather.ui.weather.model

data class Rain(
    val `3h`: Double
)