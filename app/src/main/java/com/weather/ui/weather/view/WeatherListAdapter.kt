package com.weather.ui.weather.view

import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.recyclerview.widget.RecyclerView
import com.weather.R
import com.weather.ui.weather.model.DataList
import com.weather.ui.weatherinfo.view.WeatherInfoActivity
import com.weather.utils.*

class WeatherListAdapter(private val dataSet: List<DataList>,
                         private val context: Context,
                         private val cityName:String) :
    RecyclerView.Adapter<WeatherListAdapter.ViewHolder>() {

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val driverName: TextView
        val temp: TextView
        val weatherView: CardView

        init {
            // Define  listener for the ViewHolder's View.
            driverName = view.findViewById(R.id.weather_name)
            temp = view.findViewById(R.id.temp_name)
            weatherView = view.findViewById(R.id.weather_view)
        }
    }

    override fun onCreateViewHolder(viewGroup: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(viewGroup.context)
            .inflate(R.layout.item_weather_list, viewGroup, false)

        return ViewHolder(view)
    }

    override fun onBindViewHolder(viewHolder: ViewHolder, position: Int) {
        viewHolder.driverName.text = dataSet[position].weather.firstOrNull()?.main ?:""
        viewHolder.temp.text = "Temp ${dataSet[position].main.temp.convertKelvinToFarenhit()}"
        viewHolder.weatherView.setOnClickListener {
            val intent = Intent(context, WeatherInfoActivity::class.java)
            intent.putExtra(WEATHER_DATA, dataSet[position].weather.firstOrNull()?.description ?:"")
            intent.putExtra(TEMP,dataSet[position].main.temp)
            intent.putExtra(TEMP_FEELS_LIKE, dataSet[position].main.feels_like)
            intent.putExtra(WEATHER, dataSet[position].weather.firstOrNull()?.main ?:"")
            intent.putExtra(CITYNAME, cityName)
            context.startActivity(intent)
        }
    }

    override fun getItemCount() = dataSet.size

}