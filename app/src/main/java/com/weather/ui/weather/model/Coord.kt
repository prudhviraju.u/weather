package com.weather.ui.weather.model

data class Coord(
    val lat: Double,
    val lon: Double
)