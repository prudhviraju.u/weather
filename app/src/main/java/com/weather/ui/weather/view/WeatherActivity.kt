package com.weather.ui.weather.view

import android.os.Bundle
import androidx.recyclerview.widget.LinearLayoutManager
import com.weather.base.BaseActivity
import com.weather.data.api.Status
import com.weather.databinding.ActivityWeatherBinding
import com.weather.ui.weather.model.DataList
import com.weather.ui.weather.viewmodel.WeatherViewModel
import com.weather.utils.CITYNAME
import com.weather.utils.showToast

class WeatherActivity : BaseActivity() {
    private lateinit var mBinding: ActivityWeatherBinding
    private lateinit var mViewModel: WeatherViewModel
    private lateinit var adapter : WeatherListAdapter
    private var weatherList = ArrayList<DataList>()
    private lateinit var cityName:String

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mBinding = ActivityWeatherBinding.inflate(layoutInflater)
        setContentView(mBinding.root)
        cityName = intent.getStringExtra(CITYNAME)?:""
        setUpViewModel()
        setupObservers()
        setupRecyclerView()
        setupTitle()
    }

    private fun setupTitle(){
        supportActionBar?.title = cityName
    }

    private fun setUpViewModel(){
        mViewModel = WeatherViewModel(cityName)
    }

    private fun setupRecyclerView() {
        adapter =  WeatherListAdapter(weatherList,this, cityName)
        mBinding.weatherList.layoutManager = LinearLayoutManager(applicationContext)
        mBinding.weatherList.adapter = adapter
    }

    private fun setupObservers() {
        mViewModel.getWeatherData().observe(this) {
            when (it.status) {
                Status.SUCCESS -> {
                    hideProgressBar()
                    it.data?.let { dataList ->
                        weatherList.addAll(dataList)
                        adapter.notifyDataSetChanged()
                    }
                }
                Status.LOADING -> {
                    showProgressBar()
                }
                else -> {
                    hideProgressBar()
                    showToast(this,"No Data Found")
                }
            }
        }
    }
}