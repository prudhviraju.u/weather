package com.weather.data.api

enum class Status {
    SUCCESS,
    ERROR,
    LOADING
}