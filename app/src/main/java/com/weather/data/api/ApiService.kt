package com.weather.data.api

import com.weather.ui.weather.model.WeatherData
import retrofit2.http.GET
import retrofit2.http.Query

interface ApiService {

    @GET("data/2.5/forecast?")
    suspend fun getForeCast(@Query("q") q:String, @Query("appid") appId:String): WeatherData
}